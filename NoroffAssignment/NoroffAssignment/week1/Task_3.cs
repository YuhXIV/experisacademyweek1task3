﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    public static class Task_3
    {
        /// <summary>
        /// The main function that runs the promps
        /// </summary>
        public static void Square()
        {
            #region Variable
            string input;
            int size;
            bool successParsed;
            #endregion

            #region console prompt
            do
            {
                Console.WriteLine("Please enter the size of the square(NxN and max size 50) in integer(zero not allowed): ");
                input = Console.ReadLine();
                successParsed = Int32.TryParse(input, out size);

            } while (!successParsed || size == 0 || size > 50);
            #endregion

            CreateSquare(size);
        }

        /// <summary>
        /// Create/draw the square in the console with hashtag character (#)
        /// </summary>
        /// <param name="size">NxN</param>
        static void CreateSquare(int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (j == 0 || j == size - 1 || i == 0 || i == size - 1) Console.Write("# ");
                    else Console.Write("  ");
                }
                Console.WriteLine(" ");
            }
        }

    }
}
