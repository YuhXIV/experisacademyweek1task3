﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    class Task_4
    {
        /// <summary>
        /// The method that runs all the necessary prompt and handle all the input
        /// </summary>
        public void NestedRectangle()
        {
            #region Variable
            string input;
            int row;
            int col;
            bool successParsed;
            int max = 50;
            int min = 7;
            #endregion

            #region Console prompt
            do
            {
                do
                {
                    Console.WriteLine($"Please enter the number row of the rectangle(NxN) in integer(Min {min}, Max {max}): ");
                    input = Console.ReadLine();
                    successParsed = Int32.TryParse(input, out row); //parse the input to int, if the input is not number return false

                } while (!successParsed || row < min || row > max);

                do
                {
                    Console.WriteLine($"Please enter the number col of the rectangle(NxN) in integer(Min {min}, Max {max}): ");
                    input = Console.ReadLine();
                    successParsed = Int32.TryParse(input, out col);

                } while (!successParsed || col < min || col > max);
                if (row == col) Console.WriteLine("Not allowed to make a square"); // make sure its a rectangle not square
            } while (row == col);
            #endregion

            CreateRectangle(row, col);
        }

        /// <summary>
        /// Creates both the outer rectangle and the inner rectangle
        /// Printed out in console
        /// </summary>
        /// <param name="row">number of rows</param>
        /// <param name="col">number of columns</param>
        public void CreateRectangle(int row, int col) 
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (j == 0 || j == col - 1 || i == 0 || i == row - 1) Console.Write("# "); // the first/outer rectangle
                    else if (j == 2 && i != 1 && i != row - 2
                        || j == col - 3 && i != row - 2 && i != 1
                        || i == 2 && j != 1 && j != col - 2
                        || i == row - 3 && j != col - 2 && j != 1) // The inner rectangle (second)
                        Console.Write("# ");
                    else Console.Write("  ");
                }
                Console.WriteLine(" ");
            }
        }
    }
}
